package com.shpp.pzobenko.dto;

import com.shpp.pzobenko.validator.addresses.ValidateDTOAddress;
import com.shpp.pzobenko.validator.category.ValidateDTOCategory;
import com.shpp.pzobenko.validator.nameofgoods.ValidateDTOName;

public class DataTransferObject {

    public DataTransferObject() {
        //For initialization only
    }

    @ValidateDTOName
    private String nameOfGood;
    @ValidateDTOCategory
    private int categoryId;
    @ValidateDTOAddress
    private int addressOfShop;

    /**
     * Get name of good
     *
     * @return name of good
     */
    public String getNameOfGood() {
        return nameOfGood;
    }

    /**
     * Set name of good
     * @param nameOfGood name of good to set
     */
    public void setNameOfGood(String nameOfGood) {
        this.nameOfGood = nameOfGood;
    }

    /**
     * Get category id
     *
     * @return category id
     */
    public int getCategoryId() {
        return categoryId;
    }

    /**
     * Set category id
     * @param categoryId category id to set
     */
    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    /**
     * Get address of shop
     * @return address of shop
     */
    public int getAddressOfShop() {
        return addressOfShop;
    }

    /**
     * Set address of shop
     * @param addressOfShop address of shop set
     */
    public void setAddressOfShop(int addressOfShop) {
        this.addressOfShop = addressOfShop;
    }
}
