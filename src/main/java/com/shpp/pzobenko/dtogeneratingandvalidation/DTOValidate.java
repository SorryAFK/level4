package com.shpp.pzobenko.dtogeneratingandvalidation;

import com.shpp.pzobenko.dto.DataTransferObject;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

public class DTOValidate {
    private DTOValidate() {
        //need to be empty
    }

    /**
     * Validate DTOs on array, if dto not valid call method which make recursion which generate new dto
     * It need to do not create new stream
     *
     * @param dtoS array of DTOs
     */
    public static void validate(DataTransferObject[] dtoS) {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        for (int i = 0; i < dtoS.length; i++) {
            dtoS[i] = checkDTO(dtoS[i], validator);
        }
        factory.close();
    }

    private static DataTransferObject checkDTO(DataTransferObject dataTransferObject, Validator validator) {
        if (validator.validate(dataTransferObject).isEmpty()) {
            return dataTransferObject;
        } else {
            return checkDTO(DTOGenerating.generateDto(new DataTransferObject()), validator);
        }
    }
}
