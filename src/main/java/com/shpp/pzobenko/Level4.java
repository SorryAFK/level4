package com.shpp.pzobenko;

import com.shpp.pzobenko.jdbc.Connection;
import com.shpp.pzobenko.constandprop.Prop;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Create a tables, fuel tables with values and return the epicenter address with biggest num of selected category
 */
public class Level4 {

    private static final Logger log = LoggerFactory.getLogger(Level4.class);

    public static void main(String[] args) {
        if (args.length == 0) {
            log.error("Write arguments on console.");
        } else {
            log.info("Make connection to DB...");
            Connection.connection(new Prop(), args[0]);
        }
    }
}