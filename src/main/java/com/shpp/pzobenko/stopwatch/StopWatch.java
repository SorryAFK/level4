package com.shpp.pzobenko.stopwatch;

public class StopWatch {

    private final Long watch;
    private static final int DIVIDE_TO_MAKE_SEC = 1000;

    public StopWatch() {
        watch = System.currentTimeMillis();
    }

    public Long getTimeInSeconds() {
        return ((System.currentTimeMillis() - watch) / DIVIDE_TO_MAKE_SEC);
    }
    public Long getTimeInMSeconds() {return (System.currentTimeMillis() - watch);}
}
