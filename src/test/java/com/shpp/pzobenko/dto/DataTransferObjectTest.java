package com.shpp.pzobenko.dto;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DataTransferObjectTest {

    static DataTransferObject dto;

    @BeforeAll
    static void fuelDTO() {
        dto = new DataTransferObject();
    }

    @Test
    void setNGetNameOfGood() {
        String expected = "Test";
        dto.setNameOfGood("Test");
        assertEquals(expected, dto.getNameOfGood());
    }

    @Test
    void setNGetCategoryId() {
        int expected = 10;
        dto.setCategoryId(10);
        assertEquals(expected, dto.getCategoryId());
    }

    @Test
    void setNGetAddressOfShop() {
        int expected = 1;
        dto.setAddressOfShop(1);
        assertEquals(expected, dto.getAddressOfShop());
    }
}