package com.shpp.pzobenko.dtogeneratingandvalidation;

import com.shpp.pzobenko.constandprop.Constants;
import com.shpp.pzobenko.dto.DataTransferObject;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DTOGeneratingTest {

    @Test
    void generate() {
        DataTransferObject[] array = DTOGenerating.generate();
        int expected = Constants.NUMBER_OF_GOODS_PER_STREAM;
        assertEquals(expected,array.length);
    }

    @Test
    void generateDto() {
        DataTransferObject dto = DTOGenerating.generateDto(new DataTransferObject());
        assertNotNull(dto);
    }
}