package com.shpp.pzobenko.constandprop;

import com.shpp.pzobenko.Level4;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Properties;

/**
 * Loading properties from "prop.properties" file and creating getters for everyone
 */
public class Prop {
    private String password;
    private String username;
    private String url;
    private String ddlScriptIndex;
    private String ddlScriptForEpicenter;
    private String ddlScriptForCategories;
    private String ddlScriptForGoods;
    private String ddlScriptDrop;
    private String ddlScriptAlter;
    private String driver;

    public Prop() {
        loadProp();
    }

    private static final Logger log = LoggerFactory.getLogger(Prop.class);

    private void loadProp() {
        log.info("Load properties...");
        Properties prop = new Properties();
        try {
            prop.load(Level4.class.getClassLoader().getResourceAsStream("prop.properties"));
        } catch (IOException e) {
            log.error("Can`t find properties file on resource");
        }
        password = prop.getProperty("password");
        username = prop.getProperty("username");
        url = prop.getProperty("dbUrl");
        driver = prop.getProperty("jdbcDriver");
        ddlScriptForEpicenter = prop.getProperty("ddlScriptForEpicenter");
        ddlScriptForCategories = prop.getProperty("ddlScriptForCategories");
        ddlScriptForGoods = prop.getProperty("ddlScriptForGoods");
        ddlScriptDrop = prop.getProperty("ddlScriptDrop");
        ddlScriptAlter = prop.getProperty("ddlScriptAlter");
        ddlScriptIndex = prop.getProperty("ddlScriptIndex");
    }

    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }

    public String getUrl() {
        return url;
    }

    public String getDdlScriptAlter() {
        return ddlScriptAlter;
    }

    public String getDriver() {
        return driver;
    }

    public String getDdlScriptForEpicenter() {
        return ddlScriptForEpicenter;
    }

    public String getDdlScriptForCategories() {
        return ddlScriptForCategories;
    }

    public String getDdlScriptForGoods() {
        return ddlScriptForGoods;
    }

    public String getDdlScriptIndex() {
        return ddlScriptIndex;
    }

    public String getDdlScriptDrop() {
        return ddlScriptDrop;
    }
}