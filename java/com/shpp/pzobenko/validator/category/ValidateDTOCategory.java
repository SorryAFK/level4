package com.shpp.pzobenko.validator.category;

import com.shpp.pzobenko.constandprop.Constants;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = ValidateDTOCategoryOfGoods.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidateDTOCategory {
    String message() default "The column 'category' have size more than " + Constants.CATEGORY_SIZE + " or equals 0";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
