package com.shpp.pzobenko.dtogeneratingandvalidation;

import com.shpp.pzobenko.constandprop.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CreateTables {

    private static final Logger log = LoggerFactory.getLogger(CreateTables.class);

    private CreateTables() {
    }

    public static String createTable(String tableFormula, String tableName, String[] valuesForInsert) {
        log.info("Creating table {}", tableName);
        StringBuilder builder = new StringBuilder();
        builder.append(tableFormula).append(Constants.INSERT_INTO).append(tableName).append(Constants.VALUES);
        int idCounter = 1;
        for (String values : valuesForInsert) {
            builder.append("(").append(idCounter).append(",").append(values).append("),");

            idCounter++;
        }
        builder.replace(builder.length() - 1, builder.length(),";");
        return builder.toString();
    }
}
