package com.shpp.pzobenko.dtogeneratingandvalidation;

import com.shpp.pzobenko.constandprop.Constants;
import com.shpp.pzobenko.constandprop.Prop;
import com.shpp.pzobenko.dto.DataTransferObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class SQLCommandWriter {

    /**
     * Need to be empty.
     */
    private SQLCommandWriter() {
    }

    public static String createTables(Prop prop) {
        return prop.getDdlScriptDrop() + CreateTables.createTable(prop.getDdlScriptForEpicenter(),
                Constants.NAME_OF_EPICENTER_TABLE, Constants.getAddress())
                + CreateTables.createTable(prop.getDdlScriptForCategories(), Constants.NAME_OF_CATEGORIES_TABLE,
                Constants.getCategory()) +
                prop.getDdlScriptForGoods();
    }
    /**
     * Creating select request for table products on id_epicenter column
     *
     * @param nameOfCategory needs to take only columns where have this category
     * @return List of requests
     */
    public static List<String> selectMaxValueByCategory(String nameOfCategory) {
        List<String> arrayOfCommands = new ArrayList<>();
        StringBuilder buildSelect = new StringBuilder();
        for (String address : Constants.getAddress()) {
            buildSelect.append(Constants.SELECT_MESSAGE1).append(nameOfCategory).
                    append(Constants.SELECT_MESSAGE2).append(address).append(";");
            arrayOfCommands.add(buildSelect.toString());
            buildSelect.replace(0, buildSelect.length(), "");
        }
        return arrayOfCommands;
    }

    /**
     * Creating insert commands with dto to fuel products table;
     *
     * @param stringBuilder giving this StringBuilder as parameter needs to not create a new one each time.
     * @param count         counting the num of giving commands.
     * @return sql command to insert values.
     */
    public static String createInsertCommands(StringBuilder stringBuilder, AtomicInteger count) {
        DataTransferObject[] dtoS = DTOGenerating.generate();
        DTOValidate.validate(dtoS);
        stringBuilder.append(Constants.PRODUCTS_TABLE);
        for (int i = 0; i < dtoS.length; i++) {
            DataTransferObject dto = dtoS[i];
            stringBuilder.append("(").append(count.get() * Constants.NUMBER_OF_GOODS_PER_STREAM + i + 1).append(", ").
                    append(dto.getNameOfGood()).append(", ").append(dto.getCategoryId()).append(", ").
                    append(dto.getAddressOfShop()).append("),");
        }
        stringBuilder.replace(stringBuilder.length() - 1, stringBuilder.length(), ";");
        return stringBuilder.toString();
    }
}